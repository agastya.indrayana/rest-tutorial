from rest_framework import serializers
from bioskop.models import Movie, Studio, Studio_type, Schedule

class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = ('id', 'name', 'duration','rating', 'synopsis', 'type')

class StudioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Studio
        fields = ('id', 'name', 'capacity')

class Studio_typeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Studio_type
        fields = ('id', 'studio_name', 'type')

class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = ('id', 'movie', 'studio', 'start_datetime', 'end_datetime')