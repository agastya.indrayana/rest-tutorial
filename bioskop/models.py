from django.db import models

class Movie(models.Model):
    name = models.CharField(max_length=50)
    duration = models.IntegerField()
    rating = models.CharField(max_length=10)
    synopsis = models.TextField()
    type = models.CharField(max_length=10)

class Studio(models.Model):
    name =  models.CharField(max_length=50)
    capacity = models.IntegerField()

class Studio_type(models.Model):
    studio_name = models.ForeignKey(Studio, on_delete=models.CASCADE)
    type = models.CharField(max_length=10)

class Schedule(models.Model):
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    studio = models.ForeignKey(Studio, on_delete=models.CASCADE)
    start_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField()