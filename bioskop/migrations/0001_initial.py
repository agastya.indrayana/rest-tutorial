# Generated by Django 2.1.1 on 2019-04-14 04:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Movie',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('duration', models.IntegerField()),
                ('rating', models.CharField(max_length=10)),
                ('synopsis', models.TextField()),
                ('type', models.CharField(max_length=10)),
            ],
        ),
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_datetime', models.DateTimeField()),
                ('end_datetime', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Studio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('capacity', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Studio_type',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(max_length=10)),
                ('Studio_name', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bioskop.Studio')),
            ],
        ),
        migrations.AddField(
            model_name='schedule',
            name='Studio',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bioskop.Studio'),
        ),
        migrations.AddField(
            model_name='schedule',
            name='movie',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bioskop.Movie'),
        ),
    ]
