from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from bioskop.models import Movie, Studio, Studio_type, Schedule
from bioskop.serializers import MovieSerializer, StudioSerializer, Studio_typeSerializer, ScheduleSerializer

@api_view